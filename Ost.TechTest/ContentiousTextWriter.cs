﻿using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ost.TechTest
{
    /// <summary>
    /// Uses a <see cref="ReaderWriterLockSlim"/> to manage concurrent writing to the 
    /// text writer specified in the <see cref="ContentiousTextWriter"/> constructor.
    /// </summary>
    /// <remarks>
    /// For the purpose of the test, only the <see cref="WriteLine"/> method is implemented.
    /// <para>
    /// When a writer is opened as buffered (<see cref="OpenForBufferedWrite"/>), a <see cref="StringBuilder"/> is used 
    /// to maintain content until a threshold is reached or exceeded (<see cref="WRITE_AT_BYTES"/>).
    /// This has the effect of double buffering as writes via <see cref="TextWriter"/> are buffered by the OS.
    /// </para>
    /// </remarks>
    public class ContentiousTextWriter : TextWriter
    {
        public static TextWriter OpenForWrite(string fileSpec)
        {
            return Open(fileSpec, false);
        }

        public static TextWriter OpenForBufferedWrite(string fileSpec)
        {
            return Open(fileSpec, true);
        }

        private static ContentiousTextWriter Open(string fileSpec,
                                                  bool buffered)
        {
            StreamWriter innerWriter = new StreamWriter(fileSpec, false, Encoding.UTF8);
            return new ContentiousTextWriter(innerWriter, buffered);
        }

        private const int WRITE_AT_BYTES = 8192;

        private readonly TextWriter target;
        private readonly bool buffered;
        private readonly ReaderWriterLockSlim readWriteLock;

        private readonly StringBuilder buffer;

        public ContentiousTextWriter(TextWriter target,
                                     bool buffered)
        {
            this.target = target;
            this.buffered = buffered;

            this.readWriteLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
            this.buffer = new StringBuilder((int) (WRITE_AT_BYTES * 1.1));
        }

        public override void WriteLine(string value)
        {
            if (this.buffered)
            {
                lock (this.buffer)
                {
                    this.buffer.AppendLine(value);

                    if (this.buffer.Length >= WRITE_AT_BYTES)
                    {
                        this.WriteAll(this.buffer.ToString());
                        this.buffer.Clear();
                    }
                }
            }
            else
            {
                this.WriteAll(value);
            }
        }

        private void WriteAll(string value)
        {
            this.readWriteLock.EnterWriteLock();

            try
            {
                this.target.WriteLine(value);
            }
            finally
            {
                this.readWriteLock.ExitWriteLock();
            }
        }

        public override void Close()
        {
            base.Close();

            this.target.Close();
        }

        public override void Flush()
        {
            base.Flush();

            this.target.Flush();
        }

        public override Task FlushAsync()
        {
            return this.target.FlushAsync();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                this.readWriteLock.Dispose();
                this.target.Dispose();
            }
        }

        public override Encoding Encoding
        {
            get
            {
                return this.target.Encoding;
            }
        }
    }
}