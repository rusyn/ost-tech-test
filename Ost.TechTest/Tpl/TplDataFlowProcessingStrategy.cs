﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Ost.TechTest.ChainOfCommand;

namespace Ost.TechTest.Tpl
{
    /// <summary>
    /// Uses the Microsoft TPL library to write the data.
    /// </summary>
    public class TplDataFlowProcessingStrategy : IFileProcessingStrategy
    {
        //TODO: More investigation:
        //      Initial tests show the processing to be slightly slower vs. those in the Chain of Responsibility 
        //      (1024K records: 6.2s vs 5.8s)
        //      This is caused by the extraction of the ID data information (gender, etc.)
        //
        //      Having extracted that particular processing, I suspect it's related to the number of available consumers.
        //      Additional test: TPL + Parallel.ForEach may be quite effective.

        public void Process(ApplicationArguments launchArguments)
        {
            //TODO: Experiment with bounded capacities, consumer count.
            const int DEFAULT_CAPACITY = 512;

            TextWriter invalidOutput =
                ContentiousTextWriter.OpenForBufferedWrite(launchArguments.InvalidIdOutputFile);

            TextWriter validOutput =
                ContentiousTextWriter.OpenForBufferedWrite(launchArguments.ValidIdOutputFile);

            try
            {
                DataflowBlockOptions dataflowBlockOptions = new DataflowBlockOptions
                    {
                        BoundedCapacity = DEFAULT_CAPACITY
                    };

                BufferBlock<string> rawFileData =
                    new BufferBlock<string>(dataflowBlockOptions);

                BufferBlock<string> validRecords =
                    new BufferBlock<string>(dataflowBlockOptions);

                Task readRawData =
                    ReadRawData(launchArguments.InputFile,
                                rawFileData);

                Task processIds =
                    ProcessIds(rawFileData,
                               invalidOutput,
                               validRecords);

                Task writeValidIds =
                    WriteValidRecords(validRecords,
                                      validOutput);

                Task.WaitAll(readRawData, processIds, writeValidIds);
            }
            finally
            {
                validOutput.Flush();
                validOutput.Dispose();

                invalidOutput.Flush();
                invalidOutput.Dispose();
            }
        }

        private static async Task ReadRawData(string inputFile,
                                              ITargetBlock<string> rawFileData)
        {
            IEnumerable<string> readLines = File.ReadLines(inputFile);

            foreach (string readLine in readLines)
            {
                await rawFileData.SendAsync(readLine);
            }

            rawFileData.Complete();
        }

        private static async Task ProcessIds(ISourceBlock<string> rawData,
                                             TextWriter invalidIdTextWriter,
                                             ITargetBlock<string> validRecords)
        {
            while (await rawData.OutputAvailableAsync())
            {
                string rawRecord = await rawData.ReceiveAsync();

                if (RsaIDNumber.IsValid(rawRecord))
                {
                    await validRecords.SendAsync(rawRecord);
                }
                else
                {
                    invalidIdTextWriter.WriteLine(rawRecord);
                }
            }

            validRecords.Complete();
        }

        private static async Task WriteValidRecords(ISourceBlock<string> validRecords,
                                                    TextWriter validIdTextWriter)
        {
            while (await validRecords.OutputAvailableAsync())
            {
                string rawRecord = await validRecords.ReceiveAsync();

                LineContext lineContext = new LineContext
                    {
                        RawInput = rawRecord
                    };

                RsaIDNumber.Decompose(rawRecord, lineContext);

                string content = string.Format("{0},{1},{2},{3}",
                                               lineContext.IdNumber,
                                               lineContext.DateOfBirth.ToString("d MMM yyyy"),
                                               lineContext.Gender,
                                               lineContext.Citizenship);

                validIdTextWriter.WriteLine(content);
            }
        }
    }
}