﻿using System;

namespace Ost.TechTest
{
    internal static class Guardian
    {
        public static void NotNullableArgument(string name,
                                               object parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(name);
            }
        }
    }
}