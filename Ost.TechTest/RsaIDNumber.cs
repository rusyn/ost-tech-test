﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Ost.TechTest.ChainOfCommand;

namespace Ost.TechTest
{
    public class RsaIDNumber
    {
        private const int ASCII_ZERO = '0';

        private const int LAST_DOB_CHAR_INDEX = 5;
        private const int GENDER_CHAR_INDEX = 6;
        private const int CITIZENSHIP_CHAR_INDEX = 10;
        private const int CHECKSUM_CHAR_INDEX = 12;

        private const int CITIZENSHIP_RSA = 0;
        private const int CITIZENSHIP_OTHER = 1;

        private const int GENDER_FEMALE_START = 0;
        private const int GENDER_FEMALE_END = 4;

        private const string DATE_OF_BIRTH_FORMAT = "yyMMdd";

        private static readonly Regex VALID_ID_CONTENT =
            new Regex(@"^\d{13}$",
                      RegexOptions.Compiled);

        public static bool IsValid(string input)
        {
            return IsContentValid(input)
                   && IsDateOfBirthValid(input)
                   && IsCitizenshipValid(input)
                   && IsCheckSumValid(input);
        }

        public static void Decompose(string input, LineContext data)
        {
            string datePortion = string.Empty;

            data.IdNumber = data.RawInput;

            for (int i = 0; i < input.Length; i++)
            {
                string character = input.Substring(i, 1);
                int value = input[i] - '0';

                if (i <= LAST_DOB_CHAR_INDEX)
                {
                    datePortion += character;
                }
                else if (i == GENDER_CHAR_INDEX)
                {
                    Gender gender = DetermineGender(value);
                    data.Gender = gender.ToString();
                }
                else if (i == CITIZENSHIP_CHAR_INDEX)
                {
                    Citizenship citizenship = DetermineCitizenship(value);
                    data.Citizenship = citizenship.ToString();
                }
            }

            data.DateOfBirth = DetermineDateOfBirth(datePortion);
        }

        private static bool IsContentValid(string input)
        {
            return VALID_ID_CONTENT.IsMatch(input);
        }

        private static bool IsDateOfBirthValid(string input)
        {
            string dateOfBirth = input.Substring(0, LAST_DOB_CHAR_INDEX + 1);
            DateTime ignored;

            bool wasParsed = DateTime.TryParseExact(dateOfBirth,
                                                    DATE_OF_BIRTH_FORMAT,
                                                    Thread.CurrentThread.CurrentCulture.DateTimeFormat,
                                                    DateTimeStyles.AssumeLocal,
                                                    out ignored);

            return wasParsed;
        }

        private static bool IsCitizenshipValid(string input)
        {
            int citizenship = input[CITIZENSHIP_CHAR_INDEX] - '0';
            return citizenship == CITIZENSHIP_OTHER
                   || citizenship == CITIZENSHIP_RSA;
        }

        private static bool IsCheckSumValid(string input)
        {
            int oddDigitsSum = 0;
            string evenDigitsConcat = string.Empty;

            for (int i = 0; i < input.Length - 1; i++)
            {
                bool isEvenPoint = i % 2 == 0;

                if (isEvenPoint)
                {
                    oddDigitsSum += input[i] - ASCII_ZERO;
                }
                else
                {
                    evenDigitsConcat += input.Substring(i, 1);
                }
            }

            int evenDigitsSum = int.Parse(evenDigitsConcat) * 2;
            int nextStep = evenDigitsSum.ToString(CultureInfo.InvariantCulture)
                                        .ToCharArray()
                                        .Sum(x => x - ASCII_ZERO);

            int sum = oddDigitsSum + nextStep;
            int checkSum = 10 - (sum > 9 ? (sum % 10) : sum);
            if (checkSum == 10)
            {
                checkSum = 0;
            }

            bool checkSumsMatch = input[CHECKSUM_CHAR_INDEX] - ASCII_ZERO == checkSum;

            return checkSumsMatch;
        }

        private static Gender DetermineGender(int value)
        {
            Gender gender = value >= GENDER_FEMALE_START && value <= GENDER_FEMALE_END
                                ? Gender.Female
                                : Gender.Male;

            return gender;
        }

        private static Citizenship DetermineCitizenship(int value)
        {
            return value == CITIZENSHIP_RSA ? Citizenship.RSA : Citizenship.Other;
        }

        private static DateTime DetermineDateOfBirth(string datePortion)
        {
            DateTime result;
            DateTime.TryParseExact(
                datePortion,
                DATE_OF_BIRTH_FORMAT,
                Thread.CurrentThread.CurrentCulture.DateTimeFormat,
                DateTimeStyles.AssumeLocal,
                out result);

            return result;
        }
    }
}