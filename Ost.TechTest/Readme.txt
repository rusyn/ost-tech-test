﻿Tech Test

The objective of the test is to display capabilities in the following areas:
· Test Driven Development
· Performant coding
· Design and planning
· Efficiency

Scenario:
You will be supplied with a list of South African id numbers as a text file, each number separated by a newline.
 
You are required to parse the the 13 digit number, and supply the following:
· Validity
· Date of birth
· Gender
· Citizenship
 
You are required to process the entire list concurrently, and write the following results:
· List of valid id numbers, with the breakdown as per the above validation, for example: 7808215185082,21 Aug 1978,Male,SA.
· List of invalid id number, without the breakdown.
 
Rules:
· You have to do the validation, then the breakdown, then write to file
· You are only allowed to have a single writer to the valid result file.
· You are only allowed to have a single writer to the invalid result file.
· Code must be written in C#
· The solution must be a console application, which uses parameters to specify the source and two destination files.

*************************************************************************************************

Selecting a solution:
- The order of the output of the processing is not required to be deterministic.
- The ID validation must be deterministic.
- Processing must be in the order:
  - Validation
  - Decomposition (date of birth, gender, citizenship)
  - File output
- Primary deliverable focuses:
  - Performance
  - Efficiency

Issues:
- File input concurrency
- Performance:
  A functional approach will improve the performance profile of the solution at the expense of 
  strict maintainability and unit-testability.
  
  The correlary of this is using a pattern that allows the individual functions to be separated into
  isolatable, loosely coupled & individually testable components. This can be achieved via the use of the
  Chain of Responsibility pattern.

  The Microsoft TPL will also be tested.

Tools used:
  ReSharper
  NCrunch
  Sparx System's Enterprise Architect

Reference:
- Numeric only
- 13 characters long
- First 6 characters should contain a valid date reference
    - For the purpose of the test, date ranges will not be tested
- Pass RSA ID validation:
    - 1st through 6th characters represent the date of birth
    - 7th digit will be 0-4 (female), 5-9 (male)
    - 8th through 10th digits are sequence numbers
    - 11th digit will be 0 (RSA citizen), or 1 (other citizen)
    - 12th digit will usually be 8 or 9
    - 13th digit will be the check digit:
      - Sum odd digits, excluding the final digit = [x]
      - Multiply the concatenated even digits by 2 = [y]
      - Sum the digits in [y], add to [x] = [z]
      - Subtract the second digit in [z], from 10 = [a]
      - [a], or the second digit in [a], must equal the last character in the ID number