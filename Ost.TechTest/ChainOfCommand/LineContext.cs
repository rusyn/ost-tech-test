using System;

namespace Ost.TechTest.ChainOfCommand
{
    /// <summary>
    /// POCO: persist information as it moves around the chain of responsibility.
    /// </summary>
    public class LineContext
    {
        public string Citizenship
        {
            get;
            set;
        }

        public string Gender
        {
            get;
            set;
        }

        public string IdNumber
        {
            get;
            set;
        }

        public string RawInput
        {
            get;
            set;
        }

        public Validity Validity
        {
            get;
            set;
        }

        public DateTime DateOfBirth
        {
            get;
            set;
        }
    }
}