namespace Ost.TechTest.ChainOfCommand
{
    public enum Validity
    {
        Unprocessed = 0,
        IsInvalid = 1,
        IsValid = 2
    }
}