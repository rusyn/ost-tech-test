using System.IO;

namespace Ost.TechTest.ChainOfCommand
{
    public class IdNumberDecomposingProcessor : ILineProcessor
    {
        public ILineProcessor NextProcessor
        {
            get;
            set;
        }

        public void Process(LineContext data,
                            TextWriter validIdTextWriter,
                            TextWriter invalidIdTextWriter)
        {
            Guardian.NotNullableArgument("data", data);
            Guardian.NotNullableArgument("validIdTextWriter", validIdTextWriter);
            Guardian.NotNullableArgument("invalidIdTextWriter", invalidIdTextWriter);

            if (data.Validity != Validity.IsValid)
            {
                return;
            }

            this.ExtractIDDetail(data);
            this.WriteID(data, validIdTextWriter);
        }

        private void ExtractIDDetail(LineContext data)
        {
            RsaIDNumber.Decompose(data.RawInput, data);
        }

        private void WriteID(LineContext data,
                             TextWriter validIdTextWriter)
        {
            string content = string.Format("{0},{1},{2},{3}",
                                           data.IdNumber,
                                           data.DateOfBirth.ToString("d MMM yyyy"),
                                           data.Gender,
                                           data.Citizenship);

            validIdTextWriter.WriteLine(content);
        }
    }
}