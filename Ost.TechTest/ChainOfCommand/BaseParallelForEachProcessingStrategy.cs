﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Ost.TechTest.ChainOfCommand
{
    /// <summary>
    /// Provides the basis for the <see cref="Parallel.ForEach{TSource}(System.Collections.Generic.IEnumerable{TSource},System.Action{TSource})"/> processing strategies.
    /// In the solution, the only variance is how the output streams are opened. This gives a
    /// basis for using the Template Method pattern.
    /// </summary>
    public abstract class BaseParallelForEachProcessingStrategy : IFileProcessingStrategy
    {
        public void Process(ApplicationArguments launchArguments)
        {
            ILineProcessor lineProcessor = new LineProcessorFactory().CreateIdValidationProcessor();
            IEnumerable<string> fileInput = File.ReadLines(launchArguments.InputFile);

            using (TextWriter validIdTextWriter =
                this.OpenWriter(launchArguments.ValidIdOutputFile))
            {
                using (TextWriter invalidIdTextWriter =
                    this.OpenWriter(launchArguments.InvalidIdOutputFile))
                {
                    Parallel.ForEach(fileInput,
                                     line =>
                                         {
                                             LineContext lineContext = new LineContext
                                                 {
                                                     RawInput = line
                                                 };

                                             lineProcessor.Process(
                                                 lineContext,
// ReSharper disable AccessToDisposedClosure
                                                 validIdTextWriter,
                                                 invalidIdTextWriter);
// ReSharper restore AccessToDisposedClosure
                                         });
                }
            }
        }

        protected abstract TextWriter OpenWriter(string file);
    }
}