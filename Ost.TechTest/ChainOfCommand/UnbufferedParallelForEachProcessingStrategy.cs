﻿using System.IO;

namespace Ost.TechTest.ChainOfCommand
{
    /// <summary>
    /// Uses the <see cref="ContentiousTextWriter"/> writer to manage concurrent writes to the 
    /// output file. Content is written immediately to the text writer.
    /// </summary>
    public class UnbufferedParallelForEachProcessingStrategy : BaseParallelForEachProcessingStrategy
    {
        protected override TextWriter OpenWriter(string file)
        {
            return ContentiousTextWriter.OpenForWrite(file);
        }
    }
}