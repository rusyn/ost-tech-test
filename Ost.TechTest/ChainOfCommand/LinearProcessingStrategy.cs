﻿using System.Collections.Generic;
using System.IO;

namespace Ost.TechTest.ChainOfCommand
{
    /// <summary>
    /// Processes the input in order, without threading.
    /// </summary>
    public class LinearProcessingStrategy : IFileProcessingStrategy
    {
        public void Process(ApplicationArguments launchArguments)
        {
            ILineProcessor lineProcessor = new LineProcessorFactory().CreateIdValidationProcessor();
            IEnumerable<string> fileInput = File.ReadLines(launchArguments.InputFile);

            using (TextWriter validIdTextWriter =
                ContentiousTextWriter.OpenForWrite(launchArguments.ValidIdOutputFile))
            {
                using (TextWriter invalidIdTextWriter =
                    ContentiousTextWriter.OpenForWrite(launchArguments.InvalidIdOutputFile))
                {
                    foreach (string line in fileInput)
                    {
                        LineContext lineContext = new LineContext
                            {
                                RawInput = line
                            };

                        lineProcessor.Process(lineContext,
                                              validIdTextWriter,
                                              invalidIdTextWriter);
                    }
                }
            }
        }
    }
}