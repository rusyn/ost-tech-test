﻿using System.IO;

namespace Ost.TechTest.ChainOfCommand
{
    /// <summary>
    /// Uses the <see cref="ContentiousTextWriter"/> writer to manage concurrent writes to the 
    /// output file. Content is effectively double-buffered before being written.
    /// <see cref="ContentiousTextWriter"/> for more info.
    /// </summary>
    public class BufferedParallelForEachProcessingStrategy : BaseParallelForEachProcessingStrategy
    {
        protected override TextWriter OpenWriter(string file)
        {
            return ContentiousTextWriter.OpenForBufferedWrite(file);
        }
    }
}