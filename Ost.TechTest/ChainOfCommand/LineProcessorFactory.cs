namespace Ost.TechTest.ChainOfCommand
{
    public class LineProcessorFactory
    {
        /// <summary>
        /// Creates the chain of responsibility.
        /// </summary>
        /// <returns>The first entry point into the chain.</returns>
        public ILineProcessor CreateIdValidationProcessor()
        {
            IdNumberValidatingProcessor idNumberValidatingProcessor = new IdNumberValidatingProcessor();
            IdNumberDecomposingProcessor idNumberDecomposingProcessor = new IdNumberDecomposingProcessor();

            idNumberValidatingProcessor.NextProcessor = idNumberDecomposingProcessor;

            return idNumberValidatingProcessor;
        }
    }
}