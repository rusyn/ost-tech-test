using System.IO;

namespace Ost.TechTest.ChainOfCommand
{
    public class IdNumberValidatingProcessor : ILineProcessor
    {
        public ILineProcessor NextProcessor
        {
            get;
            set;
        }

        public void Process(LineContext data,
                            TextWriter validIdTextWriter,
                            TextWriter invalidIdTextWriter)
        {
            Guardian.NotNullableArgument("data", data);
            Guardian.NotNullableArgument("validIdTextWriter", validIdTextWriter);
            Guardian.NotNullableArgument("invalidIdTextWriter", invalidIdTextWriter);

            string input = data.RawInput ?? string.Empty;
            data.Validity = RsaIDNumber.IsValid(input)
                                ? Validity.IsValid
                                : Validity.IsInvalid;

            ILineProcessor nextProcessor = this.NextProcessor;
            if (data.Validity == Validity.IsValid
                && nextProcessor != null)
            {
                this.NextProcessor.Process(data,
                                           validIdTextWriter,
                                           invalidIdTextWriter);
            }
            else
            {
                invalidIdTextWriter.WriteLine(input);
            }
        }
    }
}