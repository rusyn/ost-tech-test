using System.IO;

namespace Ost.TechTest.ChainOfCommand
{
    /// <summary>
    /// Used to identify a node within the chain of responsibility.
    /// </summary>
    public interface ILineProcessor
    {
        ILineProcessor NextProcessor
        {
            get;
            set;
        }

        void Process(LineContext data,
                     TextWriter validIdTextWriter,
                     TextWriter invalidIdTextWriter);
    }
}