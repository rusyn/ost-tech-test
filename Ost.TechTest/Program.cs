﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Ost.TechTest.ChainOfCommand;

namespace Ost.TechTest
{
    public class Program
    {
        private static void Main(string[] args)
        {
            ApplicationArguments launchArguments = ApplicationArguments.Parse(args);

            if (launchArguments == ApplicationArguments.INVALID)
            {
                ApplicationArguments.WriteUsage(Console.Out);
            }
            else
            {
                // As no there are no instructions regarding the management of exceptions,
                // the options available are to allow them to bubble (with appropriate resource 
                // management), or to suppress.
                // I would generally use a logging library (log4net) to persist the error state
                // in a production system.

                Stopwatch processingTimer = new Stopwatch();
                processingTimer.Start();

                ProcessInput(launchArguments);

                processingTimer.Stop();
                long duration = processingTimer.ElapsedMilliseconds;
                Console.WriteLine("{0} ms", duration);
            }
        }

        private static void ProcessInput(ApplicationArguments launchArguments)
        {
            // Deferred issues:
            // - File system inspection (space availability, writability, readability). These have been 
            //   left to bubble.

            // Potential problems:
            // - Should an error occur during the read or write to the streams, subsequent operations are 
            //   likely to fail.
            //   More thought would be needed to identify whether the system should attempt to recover, 
            //   how it may do so, and how it should fail more gracefully.

            IFileProcessingStrategy processingStrategy = FileProcessingStrategyFactory.Create(launchArguments);
            processingStrategy.Process(launchArguments);
        }
    }
}