﻿using System.Collections.Generic;
using System.IO;

namespace Ost.TechTest
{
    public class ApplicationArguments
    {
        public static readonly ApplicationArguments INVALID;

        private const string INPUT_PREFIX = "--in";
        private const string VALID_OUTPUT_PREFIX = "--valid";
        private const string INVALID_OUTPUT_PREFIX = "--invalid";
        private const string PROCESSOR_PREFIX = "--processor";

        static ApplicationArguments()
        {
            INVALID = new ApplicationArguments(new Dictionary<string, string>
                {
                    {
                        INPUT_PREFIX, string.Empty
                    },
                    {
                        VALID_OUTPUT_PREFIX, string.Empty
                    },
                    {
                        INVALID_OUTPUT_PREFIX, string.Empty
                    },
                    {
                        PROCESSOR_PREFIX, string.Empty
                    },
                });
        }

        public static ApplicationArguments Parse(string[] launchArguments)
        {
            if (launchArguments == null || launchArguments.Length != 8)
            {
                return INVALID;
            }

            Dictionary<string, string> parameters =
                new Dictionary<string, string>(new CaseInsensitiveEqualityComparer());

            int i = 0;
            while (i < launchArguments.Length)
            {
                string key = launchArguments[i];
                if (string.IsNullOrEmpty(key))
                {
                    return INVALID;
                }

                bool isInstruction = key.StartsWith("--");
                if (isInstruction && i <= launchArguments.Length - 2)
                {
                    string value = launchArguments[i + 1];
                    parameters[launchArguments[i]] = value;
                }

                i++;
            }

            return new ApplicationArguments(parameters);
        }

        public static void WriteUsage(TextWriter writer)
        {
            writer.WriteLine("Usage:");
            writer.Write("Ost.TechTest.exe ");
            writer.Write(INPUT_PREFIX);
            writer.Write(" \"file-spec\" ");
            writer.Write(VALID_OUTPUT_PREFIX);
            writer.Write(" \"file-spec\" ");
            writer.Write(INVALID_OUTPUT_PREFIX);
            writer.Write(" \"file-spec\" ");
            writer.Write(PROCESSOR_PREFIX);
            writer.Write(" [ ");
            writer.Write(FileProcessingStrategyFactory.GetValidProcessorsList());
            writer.Write(" ]");

            writer.WriteLine();
        }

        private readonly Dictionary<string, string> parameters;

        private ApplicationArguments(Dictionary<string, string> parameters)
        {
            this.parameters = parameters;
        }

        public string InputFile
        {
            get
            {
                return this.parameters[INPUT_PREFIX];
            }
        }

        public string ValidIdOutputFile
        {
            get
            {
                return this.parameters[VALID_OUTPUT_PREFIX];
            }
        }

        public string InvalidIdOutputFile
        {
            get
            {
                return this.parameters[INVALID_OUTPUT_PREFIX];
            }
        }

        public string Processor
        {
            get
            {
                return this.parameters[PROCESSOR_PREFIX];
            }
        }
    }
}