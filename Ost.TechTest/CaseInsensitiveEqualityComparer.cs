﻿using System;
using System.Collections.Generic;

namespace Ost.TechTest
{
    public class CaseInsensitiveEqualityComparer : IEqualityComparer<string>
    {
        public bool Equals(string x,
                           string y)
        {
            return string.Equals(x, y, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(string obj)
        {
            return obj.ToLower().GetHashCode();
        }
    }
}