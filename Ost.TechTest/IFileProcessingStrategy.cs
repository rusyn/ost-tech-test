﻿namespace Ost.TechTest
{
    public interface IFileProcessingStrategy
    {
        void Process(ApplicationArguments launchArguments);
    }
}