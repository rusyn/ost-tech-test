﻿using System;
using System.Collections.Generic;
using Ost.TechTest.ChainOfCommand;
using Ost.TechTest.Tpl;

namespace Ost.TechTest
{
    public static class FileProcessingStrategyFactory
    {
        // The registration of processors here could be managed via an IoC container.
        // This could be used to dynamically output the list of supported processors (command line arguments,
        // validation, etc.)
        private static readonly Dictionary<string, IFileProcessingStrategy> PROCESSORS;

        static FileProcessingStrategyFactory()
        {
            PROCESSORS = new Dictionary<string, IFileProcessingStrategy>(new CaseInsensitiveEqualityComparer());
            PROCESSORS.Add("parallel-unbuffered", new UnbufferedParallelForEachProcessingStrategy());
            PROCESSORS.Add("parallel-buffered", new BufferedParallelForEachProcessingStrategy());
            PROCESSORS.Add("linear", new LinearProcessingStrategy());
            PROCESSORS.Add("tpl-dataflow", new TplDataFlowProcessingStrategy());
        }

        public static string GetValidProcessorsList()
        {
            return string.Join(" | ", PROCESSORS.Keys);
        }

        /// <summary>
        /// Allows the processor to be varied based on command-line arguments.
        /// </summary>
        /// <param name="applicationArguments"></param>
        /// <returns></returns>
        public static IFileProcessingStrategy Create(ApplicationArguments applicationArguments)
        {
            IFileProcessingStrategy processor;
            if (!PROCESSORS.TryGetValue(applicationArguments.Processor,
                                        out processor))
            {
                throw new InvalidOperationException(
                    string.Format("Processor '{0}' not recognised.",
                                  applicationArguments.Processor));
            }

            return processor;
        }
    }
}