﻿using System;
using System.IO;
using Moq;
using NUnit.Framework;
using Ost.TechTest.ChainOfCommand;

namespace Ost.TechTest.Tests
{
    [TestFixture]
    public class IdNumberDecomposingProcessorFixture
    {
        public class Process
        {
            public class Context
            {
                [Test]
                public void ShouldExtractCorrectDetail()
                {
                    const string EXPECTED_OUTPUT = "8310095068081,9 Oct 1983,Male,RSA";

                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Strict);
                    validIdWriter.Setup(x => x.WriteLine(EXPECTED_OUTPUT));

                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Strict);

                    LineContext context = new LineContext();
                    context.Validity = Validity.IsValid;
                    context.RawInput = "8310095068081";

                    IdNumberDecomposingProcessor idNumberValidatingProcessor = new IdNumberDecomposingProcessor();
                    idNumberValidatingProcessor.Process(context,
                                                        validIdWriter.Object,
                                                        invalidIdWriter.Object);

                    validIdWriter.VerifyAll();
                }

                [Test]
                public void WhenNull_ShouldRaiseException()
                {
                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Strict);
                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Strict);

                    IdNumberDecomposingProcessor idNumberValidatingProcessor = new IdNumberDecomposingProcessor();

                    Assert.Throws<ArgumentNullException>(
                        () => idNumberValidatingProcessor.Process(null,
                                                                  validIdWriter.Object,
                                                                  invalidIdWriter.Object));
                }

                [Test]
                public void ShouldOnlyProcess_ValidatedContent()
                {
                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Strict);
                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Strict);
                    LineContext lineContext = new LineContext
                        {
                            Validity = Validity.IsInvalid
                        };

                    IdNumberDecomposingProcessor idNumberValidatingProcessor = new IdNumberDecomposingProcessor();
                    idNumberValidatingProcessor.Process(lineContext,
                                                        validIdWriter.Object,
                                                        invalidIdWriter.Object);

                    // Fail-safe test; previous components should log the invalid ID number.

                    validIdWriter.VerifyAll();
                    invalidIdWriter.VerifyAll();
                }
            }

            public class ValidIdTextWriter
            {
                [Test]
                public void WhenNull_ShouldRaiseException()
                {
                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Strict);

                    IdNumberDecomposingProcessor idNumberValidatingProcessor = new IdNumberDecomposingProcessor();

                    Assert.Throws<ArgumentNullException>(
                        () => idNumberValidatingProcessor.Process(new LineContext(),
                                                                  null,
                                                                  invalidIdWriter.Object));
                }
            }

            public class InvalidIdTextWriter
            {
                [Test]
                public void WhenNull_ShouldRaiseException()
                {
                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Strict);

                    IdNumberDecomposingProcessor idNumberValidatingProcessor = new IdNumberDecomposingProcessor();

                    Assert.Throws<ArgumentNullException>(
                        () => idNumberValidatingProcessor.Process(new LineContext(),
                                                                  validIdWriter.Object,
                                                                  null));
                }
            }
        }
    }
}