﻿using System;
using System.IO;
using Moq;
using NUnit.Framework;
using Ost.TechTest.ChainOfCommand;

namespace Ost.TechTest.Tests
{
    [TestFixture]
    public class IdNumberValidatingProcessorFixture
    {
        public class Process
        {
            public class Context
            {
                [Test]
                public void RawInput_ValidContent_ChecksumEquals10_ShouldMarkAsValid()
                {
                    LineContext context = this.RunInputTest("7201018689080");

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsValid));
                }

                [Test]
                public void RawInput_ValidContent_ShouldMarkAsValid()
                {
                    LineContext context = this.RunInputTest("8310095068081");

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsValid));
                }

                [Test]
                public void RawInput_InvalidCheckSum_ShouldMarkAsInvalid()
                {
                    LineContext context = this.RunInputTest("8310095068082");

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsInvalid));
                }

                [Test]
                public void RawInput_InvalidCitizenship_ShouldMarkAsInvalid()
                {
                    LineContext context = this.RunInputTest("8310095068381");

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsInvalid));
                }

                [Test]
                public void RawInput_InvalidDoB_Day_ShouldMarkAsInvalid()
                {
                    LineContext context = this.RunInputTest("8310325068081");

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsInvalid));
                }

                [Test]
                public void RawInput_InvalidDoB_Month_ShouldMarkAsInvalid()
                {
                    LineContext context = this.RunInputTest("8316095068081");

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsInvalid));
                }

                [Test]
                public void RawInput_AlphaNumeric_CorrectLength_ShouldMarkAsInvalid()
                {
                    LineContext context = this.RunInputTest("1234567890ABC");

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsInvalid));
                }

                [Test]
                public void RawInput_AlphaNumeric_IncorrectLength_ShouldMarkAsInvalid()
                {
                    LineContext context = this.RunInputTest("1234567890AB");

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsInvalid));
                }

                [Test]
                public void RawInput_Empty_ShouldMarkAsInvalid()
                {
                    LineContext context = this.RunInputTest(string.Empty);

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsInvalid));
                }

                [Test]
                public void RawInput_Null_ShouldMarkAsInvalid()
                {
                    LineContext context = this.RunInputTest(null);

                    Assert.That(context.Validity, Is.EqualTo(Validity.IsInvalid));
                }

                [Test]
                public void WhenNull_ShouldRaiseException()
                {
                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Strict);
                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Strict);

                    IdNumberValidatingProcessor idNumberValidatingProcessor = new IdNumberValidatingProcessor();

                    Assert.Throws<ArgumentNullException>(
                        () => idNumberValidatingProcessor.Process(null,
                                                                  validIdWriter.Object,
                                                                  invalidIdWriter.Object));
                }

                private LineContext RunInputTest(string rawInput)
                {
                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Loose);
                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Loose);

                    LineContext context = new LineContext();
                    context.RawInput = rawInput;

                    IdNumberValidatingProcessor idNumberValidatingProcessor = new IdNumberValidatingProcessor();
                    idNumberValidatingProcessor.Process(context,
                                                        validIdWriter.Object,
                                                        invalidIdWriter.Object);

                    return context;
                }
            }

            public class ValidTextWriter
            {
                [Test]
                public void WhenNull_ShouldRaiseException()
                {
                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Strict);

                    IdNumberValidatingProcessor idNumberValidatingProcessor = new IdNumberValidatingProcessor();

                    Assert.Throws<ArgumentNullException>(
                        () => idNumberValidatingProcessor.Process(new LineContext(),
                                                                  null,
                                                                  invalidIdWriter.Object));
                }
            }

            public class InvalidTextWriter
            {
                [Test]
                public void ShouldWriteIdNumber_IfInvalid()
                {
                    const string testIDNumber = "8310";

                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Strict);
                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Strict);
                    invalidIdWriter.Setup(x => x.WriteLine(testIDNumber));

                    LineContext context = new LineContext();
                    context.RawInput = testIDNumber;

                    IdNumberValidatingProcessor idNumberValidatingProcessor = new IdNumberValidatingProcessor();
                    idNumberValidatingProcessor.Process(context,
                                                        validIdWriter.Object,
                                                        invalidIdWriter.Object);

                    invalidIdWriter.VerifyAll();
                }

                [Test]
                public void WhenNull_ShouldRaiseException()
                {
                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Strict);

                    IdNumberValidatingProcessor idNumberValidatingProcessor = new IdNumberValidatingProcessor();

                    Assert.Throws<ArgumentNullException>(
                        () => idNumberValidatingProcessor.Process(new LineContext(),
                                                                  validIdWriter.Object,
                                                                  null));
                }
            }

            public class NextProcessor
            {
                [Test]
                public void ShouldInvokeNextProcessor_IfValid()
                {
                    // Arrange.
                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Strict);
                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Strict);

                    Mock<ILineProcessor> nextProcessor = new Mock<ILineProcessor>(MockBehavior.Strict);
                    nextProcessor.Setup(x => x.Process(It.IsAny<LineContext>(),
                                                       validIdWriter.Object,
                                                       invalidIdWriter.Object));

                    LineContext context = new LineContext();
                    context.RawInput = "8310095068081";

                    IdNumberValidatingProcessor idNumberValidatingProcessor = new IdNumberValidatingProcessor();
                    idNumberValidatingProcessor.NextProcessor = nextProcessor.Object;

                    // Act.
                    idNumberValidatingProcessor.Process(context,
                                                        validIdWriter.Object,
                                                        invalidIdWriter.Object);

                    // Assert.
                    nextProcessor.VerifyAll();
                }

                [Test]
                public void ShouldNotInvokeNextProcessor_IfInvalid()
                {
                    const string rawInput = "8310095068082";

                    // Arrange.
                    Mock<TextWriter> validIdWriter = new Mock<TextWriter>(MockBehavior.Strict);

                    Mock<TextWriter> invalidIdWriter = new Mock<TextWriter>(MockBehavior.Strict);
                    invalidIdWriter.Setup(x => x.WriteLine(rawInput));

                    Mock<ILineProcessor> nextProcessor = new Mock<ILineProcessor>(MockBehavior.Strict);

                    LineContext context = new LineContext();
                    context.RawInput = rawInput;

                    IdNumberValidatingProcessor idNumberValidatingProcessor = new IdNumberValidatingProcessor();
                    idNumberValidatingProcessor.NextProcessor = nextProcessor.Object;

                    // Act.
                    idNumberValidatingProcessor.Process(context,
                                                        validIdWriter.Object,
                                                        invalidIdWriter.Object);

                    // Assert.
                    nextProcessor.Verify(x => x.Process(It.IsAny<LineContext>(),
                                                        validIdWriter.Object,
                                                        invalidIdWriter.Object),
                                         Times.Never);
                    invalidIdWriter.VerifyAll();
                }
            }
        }
    }
}