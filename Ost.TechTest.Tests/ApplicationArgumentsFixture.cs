﻿using NUnit.Framework;

namespace Ost.TechTest.Tests
{
    [TestFixture]
    public class ApplicationArgumentsFixture
    {
        public class Parse
        {
            [Test]
            public void IncorrectArgumentsLength_ShouldReturnInvalid()
            {
                ApplicationArguments applicationArguments = 
                    ApplicationArguments.Parse(new string[0]);

                Assert.That(applicationArguments, Is.EqualTo(ApplicationArguments.INVALID));
            }

            [Test]
            public void NullArguments_ShouldReturnInvalid()
            {
                ApplicationArguments applicationArguments = 
                    ApplicationArguments.Parse(null);

                Assert.That(applicationArguments, Is.EqualTo(ApplicationArguments.INVALID));
            }

            [Test]
            public void OnlyInputFileSpecified_ShouldReturnInvalid()
            {
                ApplicationArguments applicationArguments = 
                    ApplicationArguments.Parse(new[]
                        {
                            "--in",
                            "c:\\"
                        });

                Assert.That(applicationArguments, Is.EqualTo(ApplicationArguments.INVALID));
            }

            [Test]
            public void OnlyValidOutputFileSpecified_ShouldReturnInvalid()
            {
                ApplicationArguments applicationArguments = 
                    ApplicationArguments.Parse(new[]
                        {
                            "--valid",
                            "c:\\"
                        });

                Assert.That(applicationArguments, Is.EqualTo(ApplicationArguments.INVALID));
            }

            [Test]
            public void OnlyInvalidOutputFileSpecified_ShouldReturnInvalid()
            {
                ApplicationArguments applicationArguments = 
                    ApplicationArguments.Parse(new[]
                        {
                            "--invalid",
                            "c:\\"
                        });

                Assert.That(applicationArguments, Is.EqualTo(ApplicationArguments.INVALID));
            }

            [Test]
            public void AllCommands_NoValues_ShouldReturnInvalid()
            {
                ApplicationArguments applicationArguments = 
                    ApplicationArguments.Parse(new[]
                        {
                            "--in",
                            string.Empty,
                            "--valid",
                            string.Empty,
                            "--invalid",
                            string.Empty,
                        });

                Assert.That(applicationArguments, Is.EqualTo(ApplicationArguments.INVALID));
            }

            [Test]
            public void AllCommands_WithValues_ShouldReturnValidArguments()
            {
                const string INPUT_FILE = "input_file";
                const string VALID_OUTPUT_FILE = "valid_output_file";
                const string INVALID_OUTPUT_FILE = "invalid_output_file";
                const string PROCESSOR = "processor";

                ApplicationArguments applicationArguments = 
                    ApplicationArguments.Parse(new[]
                        {
                            "--in",
                            INPUT_FILE,
                            "--valid",
                            VALID_OUTPUT_FILE,
                            "--invalid",
                            INVALID_OUTPUT_FILE,
                            "--processor",
                            PROCESSOR,
                        });

                Assert.That(applicationArguments, Is.Not.EqualTo(ApplicationArguments.INVALID));
                Assert.That(applicationArguments.InputFile, Is.EqualTo(INPUT_FILE));
                Assert.That(applicationArguments.ValidIdOutputFile, Is.EqualTo(VALID_OUTPUT_FILE));
                Assert.That(applicationArguments.InvalidIdOutputFile, Is.EqualTo(INVALID_OUTPUT_FILE));
                Assert.That(applicationArguments.Processor, Is.EqualTo(PROCESSOR));
            }
        }
    }
}