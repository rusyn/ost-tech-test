﻿using System;
using System.Diagnostics;

namespace IdNumberGenerator
{
    internal class Program
    {
        private static readonly DateTime SEED_ID_DOB = new DateTime(1972, 1, 1);
        private const int BUMP_DOB_EVERY_N = 300;

        private static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine("IdNumberGenerator [count] [error_rate] > all.txt 2> invalid.txt");
                Console.WriteLine();
                Console.WriteLine("Generates [count] of IDs, where 1 in [error_rate] are invalid.");
                Console.WriteLine("All ID's will be piped to StdOut, invalid to StdErr.");

                return;
            }

            int count = int.Parse(args[0]);
            int errorRate = int.Parse(args[1]);

            GenerateIds(count, errorRate, new Random());

            if (Debugger.IsAttached)
            {
                Console.WriteLine("\nDone.");
                Console.ReadKey(true);
            }
        }

        private static void GenerateIds(int count,
                                        int errorRate,
                                        Random random)
        {
            DateTime idDateTime = SEED_ID_DOB;

            for (int i = 0; i < count; i++)
            {
                bool generateAsInvalid = errorRate > 0
                                         && (i + 1) % errorRate == 0;

                string id = GenerateId(idDateTime, generateAsInvalid, random);
                Console.WriteLine(id);

                if (generateAsInvalid)
                {
                    Console.Error.WriteLine(id);
                }

                if ((i + 1) % BUMP_DOB_EVERY_N == 0)
                {
                    idDateTime = idDateTime.AddDays(1);
                }
            }
        }

        private static string GenerateId(DateTime idDateTime,
                                         bool generateAsInvalid,
                                         Random random)
        {
            int gender = random.Next(0, 10);
            int sequence = random.Next(0, 1000);
            int citizenship = generateAsInvalid ? random.Next(3, 10) : random.Next(2);
            int aNumber = random.Next(10);

            string idNumber = idDateTime.ToString("yyMMdd")
                              + gender
                              + sequence.ToString("000")
                              + citizenship
                              + aNumber;

            idNumber += CalculateCheckSum(idNumber, generateAsInvalid);

            return idNumber;
        }

        private static int CalculateCheckSum(string idNumber,
                                             bool generateAsInvalid)
        {
            int sumEven = 0;
            int sumOdd = 0;
            bool even = false;

            for (int i = 0; i < 12; i++)
            {
                int charValue = idNumber[i] - '0';
                if (!even)
                {
                    sumOdd += charValue;
                }
                else
                {
                    var doubleEven = 2 * charValue;
                    sumEven += (doubleEven / 10) + (doubleEven % 10);
                }

                even = !even;
            }

            int total = sumOdd + sumEven;
            int check = (10 - (total % 10)) % 10;

            if (generateAsInvalid)
            {
                check = (check + 2) % 10;
            }

            return check;
        }
    }
}