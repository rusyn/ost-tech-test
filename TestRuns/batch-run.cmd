@echo off
for /f %%G in ('dir /b all_*.txt') do (
	echo %%G
	for /f %%P in (processors) do (
		echo  %%P
		..\Ost.TechTest\bin\Debug\Ost.TechTest.exe --in %%G --valid valid_%%G --invalid invalid_%%G --processor %%P
	)
        echo.
)